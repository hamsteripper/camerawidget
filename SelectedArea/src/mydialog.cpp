#include "../include/mydialog.h"

MyDialog::MyDialog(QWidget * parent) : QDialog(parent){

    this->setWindowTitle("Correct ?");

    layout = new QVBoxLayout(this);
    this->setLayout(layout);

    yes = new QPushButton("yes");
    no = new QPushButton("no");
    layout->addWidget(yes);
    layout->addWidget(no);

    this->setFixedWidth( 200 );

    signalMapper = new QSignalMapper(this);
    signalMapper->setMapping(yes, "yes");
    signalMapper->setMapping(no, "no");
    connect(yes, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(no, SIGNAL(clicked()), signalMapper, SLOT(map()));

    connect(signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(slotButtonClick(const QString &)));

}

void MyDialog::slotButtonClick(QString str){

    if(str == "yes"){
        emit signalButtonClick(true);
        this->accept();
    }else{
        emit signalButtonClick(false);
        this->accept();
    }


}
