#include "../include/selectarea.h"

SelectedArea::SelectedArea(QWidget * parent) : QWidget(parent)
{
//    this->setWindowModified(true);
    this->setWindowTitle("Select Area");

    QRect screenGeometry = QApplication::desktop()->screenGeometry();
    int x = (screenGeometry.width() - 500) / 2;
    int y = (screenGeometry.height() - 500) / 2;
    screenGeometry = QRect(x,y, 500, 500);
    setGeometry(screenGeometry);

    mousePress = false;
    x0 = 0;
    y0 = 0;
    x1 = 0;
    y1 = 0;

    exit = false;

    this->setFocusPolicy(Qt::StrongFocus);
}

SelectedArea::~SelectedArea(){}

void SelectedArea::setImage(QImage img){
    this->img = new QImage(img);
    this->newImg = this->img;

    QRect screenGeometry = QApplication::desktop()->screenGeometry();
    int x = (screenGeometry.width() - this->img->size().width()) / 2;
    int y = (screenGeometry.height() - this->img->size().height()) / 2;
    screenGeometry = QRect(x,y, this->img->size().width(), this->img->size().height());
    setGeometry(screenGeometry);

}

void SelectedArea::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.setPen(Qt::black);
    painter.drawImage(0,0,*newImg);
    painter.drawRect(rect());

    QPen pen;
    pen.setColor(Qt::white);
    pen.setStyle(Qt::DashLine);
    painter.setPen(pen);
    painter.setBrush(QColor(100, 100, 100, 0));
    painter.drawRect(x0, y0, x1-x0, y1-y0);

    QString textHint;
    textHint = tr("Для снимка выделите область и нажмите SPACE");

    QFont font;
    font.setPixelSize(30);

    painter.setFont(font);
    painter.setPen(Qt::white);

    painter.drawText(rect(), Qt::AlignCenter, textHint);
}
void SelectedArea:: keyReleaseEvent(QKeyEvent *event){
    event->ignore();
}

void SelectedArea:: keyPressEvent(QKeyEvent *event){
    // Закрыть widget
    if(event->key() == Qt::Key_Escape){
        exit = true;
        close();
    }
    // Если нажат SPACE
    if(event->key() == Qt::Key_Space || event->key() == Qt::Key_Return){
        // Создать прямоугольник
        QRect rect(x0, y0, x1-x0, y1-y0);
        // Вырезать прямоугольник из изображения
        newImg = new QImage(img->copy(rect));
        // Отправить вырезанное изображение
        newDialog();
        //        emit areaIsSelected();
    }
}

void SelectedArea::newDialog(){

    dialog = new MyDialog();
    connect(dialog, SIGNAL(signalButtonClick(bool)), this, SLOT(yesOrNo(bool)));
    // Модальное открытие окна
    if (dialog->exec() == QDialog::Accepted){}

    // Если обрезанное изображение устраивает, то выход, иначе продолжить
    if(exit){
        close();
    }else{
        newImg = img;
    }

    delete dialog;

}

void SelectedArea::yesOrNo(bool b){
    exit = b;
}

void SelectedArea:: mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton){
        mousePress = true;
        x0 = event->x();
        y0 = event->y();

        setCursor(QCursor(Qt::CrossCursor));
    }
}
void SelectedArea:: mouseReleaseEvent(QMouseEvent *event)
{
    mousePress = false;
    setCursor(QCursor(Qt::ArrowCursor));

    if(event->button() == Qt::MiddleButton){
        close();
        emit areaIsSelected();
    }

    x1 = event->x();
    y1 = event->y();

    if(x0 > x1){
        int temp = x0;
        x0 = x1;
        x1 = temp;
    }

    if(y0 > y1){
        int temp = y0;
        y0 = y1;
        y1 = temp;
    }
}
void SelectedArea:: mouseMoveEvent(QMouseEvent *event)
{
    if(mousePress){
        x1 = event->x();
        y1 = event->y();
        update();
    }
}

void SelectedArea:: closeEvent(QCloseEvent *event)
{
    if (exit) {
        emit sendImg(*newImg);
        event->accept();
    } else {
        event->ignore();
    }
}
