#ifndef SELECTAREA_H
#define SELECTAREA_H

/*
 Этот класс наследуется от QWidget
 создает окно без рамки
 окно на весь экран и прозрачно на половину
 на этом окне можно выделять область
 при нажатии пробела, виджет подает сигнал и закрывается

 связываю этот класс с классом ScreenShot
 когда поступает сигнал, класс ScreenShot его отлавливает и
 получает координаты выделенной области, после чего делает по
 ним скриншот области экрана
 */
#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QDebug>
#include <QDate>
#include <QDateTime>
#include <QCoreApplication>
#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include "../include/mydialog.h"

class SelectedArea: public QWidget
{
    Q_OBJECT
public:
    SelectedArea(QWidget * parent = 0);
    ~SelectedArea();

private:
    bool exit;
    int x0, y0, x1, y1;
    QRect rectSelectedArea;
    bool mousePress;
    QImage *img;
    QImage *newImg;
    void newDialog(void);
    MyDialog *dialog;

signals:
    void areaIsSelected(void);
    void windowClose(void);
    void sendImg(QImage);

public slots :
    void setImage(QImage img);

private slots:
    void yesOrNo(bool b);

protected:
    void paintEvent(QPaintEvent *);
    void keyReleaseEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void closeEvent(QCloseEvent *);
    void keyPressEvent(QKeyEvent *event);
};

#endif // SELECTAREA_H
