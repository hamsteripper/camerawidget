#include <QApplication>

#include <QMainWindow>
#include <QGridLayout>
#include <QPushButton>
#include "include/camerawidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QMainWindow *w = new QMainWindow();
    w->show();


    CameraWidget *cW = new CameraWidget(w);
    w->setCentralWidget(cW);

    return a.exec();
}
