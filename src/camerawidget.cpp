#include "../include/camerawidget.h"
#include "ui_camerawidget.h"

CameraWidget::CameraWidget(QWidget *parent) : QWidget(parent), ui(new Ui::CameraWidget), camera(0), imageCapture(0), mediaRecorder(0), isCapturingImage(false), applicationExiting(false) {

    ui->setupUi(this);
    // Меню
    menuBar = new QMenuBar();

    // Подменю
    menuFile = new QMenu("Files");
    menuDevices = new QMenu("Cameras");
    // Менюшки подменю menuFile
    actionStartCamera = new QAction("Start");
    actionStopCamera = new QAction("Stop");
    action1 = new QAction();
    actionSettings = new QAction("Settings");
    action2 = new QAction();
    actionExit = new QAction("Exit");
    // Добавление менюшек подменю menuFile
    menuFile->addAction(actionStartCamera);
    menuFile->addAction(actionStopCamera);
    menuFile->addAction(action1);
    menuFile->addAction(actionSettings);
    menuFile->addAction(action2);
    menuFile->addAction(actionExit);
    // Добавление элемента подменю в меню
    menuBar->addMenu(menuFile);
    menuBar->addMenu(menuDevices);
    // Добавления меню в вертикальный виджет
    ui->boxLayout->addWidget(menuBar);

    buttonConnectors();

    ///Поиск и добавление найденных устройств (камер) в список
    // Групповые действия
    QActionGroup *videoDevicesGroup = new QActionGroup(this);
    // Только одно отмечаемое действие в группе действий может быть активно в каждый момент времени
    videoDevicesGroup->setExclusive(true);
    // Информация о камере
    foreach (const QCameraInfo &cameraInfo, QCameraInfo::availableCameras()) {
        // videoDevicesGroup родитель videoDeviceAction + идентификатор
        QAction *videoDeviceAction = new QAction(cameraInfo.description(), videoDevicesGroup);
        // Если да, то происходит переключение элементов
        videoDeviceAction->setCheckable(true);
        // Данные о камере
        videoDeviceAction->setData(QVariant::fromValue(cameraInfo));
        // Выбор камеры по умолчанию
        if (cameraInfo == QCameraInfo::defaultCamera())
            videoDeviceAction->setChecked(true);
        // Добавление элемента в меню
        menuDevices->addAction(videoDeviceAction);
    }

    connect(videoDevicesGroup, SIGNAL(triggered(QAction*)), SLOT(updateCameraDevice(QAction*)));
    connect(ui->captureWidget, SIGNAL(currentChanged(int)), SLOT(updateCaptureMode()));

    setCamera(QCameraInfo::defaultCamera());


}

void CameraWidget::buttonConnectors(){
    QObject::connect(ui->recordButton, SIGNAL(clicked()), this, SLOT(record()));
    QObject::connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(stop()));
    QObject::connect(ui->pauseButton, SIGNAL(clicked()), this, SLOT(pause()));
    QObject::connect(actionExit, SIGNAL(triggered()), this, SLOT(close()));
    QObject::connect(ui->takeImageButton, SIGNAL(clicked()), this, SLOT(takeImage()));
    QObject::connect(ui->lockButton, SIGNAL(clicked()), this, SLOT(toggleLock()));
    //    QObject::connect(ui->muteButton, SIGNAL(toggled(bool)), this, SLOT(setMuted(bool)));
    QObject::connect(ui->exposureCompensation, SIGNAL(valueChanged(int)), this, SLOT(setExposureCompensation(int)));
    //    QObject::connect(actionSettings, SIGNAL(triggered()), this, SLOT(configureCaptureSettings()));
    QObject::connect(actionStartCamera, SIGNAL(triggered()), this, SLOT(startCamera()));
    QObject::connect(actionStopCamera, SIGNAL(triggered()), this, SLOT(stopCamera()));
}

CameraWidget::~CameraWidget(){
    delete mediaRecorder;
    delete imageCapture;
    delete camera;
}


void CameraWidget::setCamera(const QCameraInfo &cameraInfo){

    delete imageCapture;
    delete mediaRecorder;
    delete camera;

    // Создать класс камера
    camera = new QCamera(cameraInfo);

    connect(camera, SIGNAL(stateChanged(QCamera::State)), this, SLOT(updateCameraState(QCamera::State)));
    connect(camera, SIGNAL(error(QCamera::Error)), this, SLOT(displayCameraError()));


    mediaRecorder = new QMediaRecorder(camera);
    connect(mediaRecorder, SIGNAL(stateChanged(QMediaRecorder::State)), this, SLOT(updateRecorderState(QMediaRecorder::State)));

    imageCapture = new QCameraImageCapture(camera);

    connect(mediaRecorder, SIGNAL(durationChanged(qint64)), this, SLOT(updateRecordTime()));
    connect(mediaRecorder, SIGNAL(error(QMediaRecorder::Error)), this, SLOT(displayRecorderError()));

    mediaRecorder->setMetaData(QMediaMetaData::Title, QVariant(QLatin1String("Test Title")));

    connect(ui->exposureCompensation, SIGNAL(valueChanged(int)), SLOT(setExposureCompensation(int)));

    camera->setViewfinder(ui->viewfinder);

    updateCameraState(camera->state());
    updateLockStatus(camera->lockStatus(), QCamera::UserRequest);
    updateRecorderState(mediaRecorder->state());

    connect(imageCapture, SIGNAL(readyForCaptureChanged(bool)), this, SLOT(readyForCapture(bool)));
    connect(imageCapture, SIGNAL(imageCaptured(int,QImage)), this, SLOT(processCapturedImage(int,QImage)));
    connect(imageCapture, SIGNAL(imageSaved(int,QString)), this, SLOT(imageSaved(int,QString)));
    connect(imageCapture, SIGNAL(error(int,QCameraImageCapture::Error,QString)), this, SLOT(displayCaptureError(int,QCameraImageCapture::Error,QString)));

    connect(camera, SIGNAL(lockStatusChanged(QCamera::LockStatus,QCamera::LockChangeReason)),
            this, SLOT(updateLockStatus(QCamera::LockStatus,QCamera::LockChangeReason)));

    ui->captureWidget->setTabEnabled(0, (camera->isCaptureModeSupported(QCamera::CaptureStillImage)));
    ui->captureWidget->setTabEnabled(1, (camera->isCaptureModeSupported(QCamera::CaptureVideo)));

    updateCaptureMode();
    camera->start();

}

void CameraWidget::keyPressEvent(QKeyEvent * event){
    if (event->isAutoRepeat())
        return;

    switch (event->key()) {
    case Qt::Key_CameraFocus:
        displayViewfinder();
        camera->searchAndLock();
        event->accept();
        break;
    case Qt::Key_Camera:
        if (camera->captureMode() == QCamera::CaptureStillImage) {
            takeImage();
        } else {
            if (mediaRecorder->state() == QMediaRecorder::RecordingState)
                stop();
            else
                record();
        }
        event->accept();
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}
void CameraWidget::displayViewfinder(){
    ui->stackedWidget->setCurrentIndex(0);
}
void CameraWidget::takeImage(){
    isCapturingImage = true;
    imageCapture->capture();
}
void CameraWidget::stop(){
    mediaRecorder->stop();
}
void CameraWidget::record(){
    mediaRecorder->record();
    updateRecordTime();
}
void CameraWidget::pause(){
    mediaRecorder->pause();
}
void CameraWidget::startCamera(){
    camera->start();
}
void CameraWidget::stopCamera(){
    camera->stop();
}
void CameraWidget::toggleLock(){
    switch (camera->lockStatus()) {
    case QCamera::Searching:
    case QCamera::Locked:
        camera->unlock();
        break;
    case QCamera::Unlocked:
        camera->searchAndLock();
    }
}

void CameraWidget::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
        return;

    switch (event->key()) {
    case Qt::Key_CameraFocus:
        camera->unlock();
        break;
    default:
        QWidget::keyReleaseEvent(event);
    }
}

void CameraWidget::closeEvent(QCloseEvent *event)
{
    if (isCapturingImage) {
        setEnabled(false);
        applicationExiting = true;
        event->ignore();
    } else {
        event->accept();
    }
}

void CameraWidget::updateCameraState(QCamera::State state)
{
    switch (state) {
    case QCamera::ActiveState:
        actionStartCamera->setEnabled(false);
        actionStopCamera->setEnabled(true);
        ui->captureWidget->setEnabled(true);
        actionSettings->setEnabled(true);
        break;
    case QCamera::UnloadedState:
    case QCamera::LoadedState:
        actionStartCamera->setEnabled(true);
        actionStopCamera->setEnabled(false);
        ui->captureWidget->setEnabled(false);
        actionSettings->setEnabled(false);
        break;
    }
}

void CameraWidget::updateRecordTime()
{
    QString str = QString("Recorded %1 sec").arg(mediaRecorder->duration()/1000);
    //    ui->statusbar->showMessage(str);
}

void CameraWidget::updateCameraDevice(QAction *action)
{
    setCamera(qvariant_cast<QCameraInfo>(action->data()));
}

void CameraWidget::updateCaptureMode()
{
    int tabIndex = ui->captureWidget->currentIndex();
    QCamera::CaptureModes captureMode = tabIndex == 0 ? QCamera::CaptureStillImage : QCamera::CaptureVideo;

    if (camera->isCaptureModeSupported(captureMode))
        camera->setCaptureMode(captureMode);
}

void CameraWidget::displayCameraError()
{
    QMessageBox::warning(this, tr("Camera error"), camera->errorString());
}

void CameraWidget::updateRecorderState(QMediaRecorder::State state)
{
    switch (state) {
    case QMediaRecorder::StoppedState:
        ui->recordButton->setEnabled(true);
        ui->pauseButton->setEnabled(true);
        ui->stopButton->setEnabled(false);
        break;
    case QMediaRecorder::PausedState:
        ui->recordButton->setEnabled(true);
        ui->pauseButton->setEnabled(false);
        ui->stopButton->setEnabled(true);
        break;
    case QMediaRecorder::RecordingState:
        ui->recordButton->setEnabled(false);
        ui->pauseButton->setEnabled(true);
        ui->stopButton->setEnabled(true);
        break;
    }
}

void CameraWidget::displayRecorderError()
{
    QMessageBox::warning(this, tr("Capture error"), mediaRecorder->errorString());
}

void CameraWidget::setExposureCompensation(int index)
{
    camera->exposure()->setExposureCompensation(index*0.5);
}

void CameraWidget::updateLockStatus(QCamera::LockStatus status, QCamera::LockChangeReason reason)
{
    QColor indicationColor = Qt::black;

    switch (status) {
    case QCamera::Searching:
        indicationColor = Qt::yellow;
        //        ui->statusbar->showMessage(tr("Focusing..."));
        ui->lockButton->setText(tr("Focusing..."));
        break;
    case QCamera::Locked:
        indicationColor = Qt::darkGreen;
        ui->lockButton->setText(tr("Unlock"));
        //        ui->statusbar->showMessage(tr("Focused"), 2000);
        break;
    case QCamera::Unlocked:
        indicationColor = reason == QCamera::LockFailed ? Qt::red : Qt::black;
        ui->lockButton->setText(tr("Focus"));
        //        if (reason == QCamera::LockFailed)
        //            ui->statusbar->showMessage(tr("Focus Failed"), 2000);
    }

    QPalette palette = ui->lockButton->palette();
    palette.setColor(QPalette::ButtonText, indicationColor);
    ui->lockButton->setPalette(palette);
}

void CameraWidget::readyForCapture(bool ready)
{
    ui->takeImageButton->setEnabled(ready);
}

void CameraWidget::processCapturedImage(int requestId, const QImage& img)
{
    Q_UNUSED(requestId);
    QImage scaledImage = img.scaled(ui->viewfinder->size(),
                                    Qt::KeepAspectRatio,
                                    Qt::SmoothTransformation);

    QGraphicsScene* scene = new QGraphicsScene();
    scene->setSceneRect(0, 0, 2, 2);
    QPainter painter(&scaledImage);
    scene->render(&painter);

    QString date = QDate::currentDate().toString("dd.MM.yyyy");
    date = date.replace(".","_");
    QString time = QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    time = time.replace(":","_");
    time = time.replace(".","_");
    //    bool b = img.save(QCoreApplication::applicationDirPath() + date + "__" + time + ".png");

    //    qDebug() << b << "\n";
    //    qDebug() << date << "\n";
    //    qDebug() << time << "\n";

    ui->lastImagePreviewLabel->setPixmap(QPixmap::fromImage(scaledImage));
//    delete selectArea;
    selectArea = new SelectedArea(this);
    selectArea->setImage(scaledImage);
    selectArea->setAttribute(Qt::WA_DeleteOnClose);
    selectArea->setWindowFlags(Qt::Dialog);
    selectArea->setWindowModality(Qt::WindowModal);
    selectArea->show();

    connect(selectArea, SIGNAL(sendImg(QImage)), this, SLOT(getImage(QImage)));

    // Display captured image for 4 seconds.
    //    displayCapturedImage();
    //    QTimer::singleShot(4000, this, SLOT(displayViewfinder()));

    qDebug() << "";
}

void CameraWidget::getImage(QImage){

    qDebug() << "image";

}

void CameraWidget::displayCapturedImage()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void CameraWidget::imageSaved(int id, const QString &fileName)
{
    Q_UNUSED(id);
    Q_UNUSED(fileName);

    isCapturingImage = false;
    if (applicationExiting)
        close();
}

void CameraWidget::displayCaptureError(int id, const QCameraImageCapture::Error error, const QString &errorString)
{
    Q_UNUSED(id);
    Q_UNUSED(error);
    QMessageBox::warning(this, tr("Image Capture Error"), errorString);
    isCapturingImage = false;
}
