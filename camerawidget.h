#ifndef CAMERAWIDGET_H
#define CAMERAWIDGET_H

#include <QObject>
#include <QWidget>
#include <QCamera>
#include <QMediaRecorder>
#include <QActionGroup>
#include <QAction>
#include <QCameraInfo>
#include <QVBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QCameraImageCapture>
#include <QMediaMetaData>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QPainter>
#include <QDate>
#include <QDateTime>
#include <QTimer>
#include <QKeyEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class CameraWidget; }
QT_END_NAMESPACE

Q_DECLARE_METATYPE(QCameraInfo)

class CameraWidget : public QWidget
{
    Q_OBJECT

    QVBoxLayout *boxLayout;
    QMenuBar* menuBar;
    QMenu *menuFile;
        QAction *actionStartCamera;
        QAction *actionStopCamera;
        QAction *action1;
        QAction *actionSettings;
        QAction *action2;
        QAction *actionExit;
    QMenu *menuDevices;

public:
    explicit CameraWidget(QWidget *parent = 0);
    ~CameraWidget();
private:
    QCamera *camera;
    QCameraImageCapture *imageCapture;
    QMediaRecorder* mediaRecorder;

    QImageEncoderSettings imageSettings;
    QAudioEncoderSettings audioSettings;
    QVideoEncoderSettings videoSettings;
    QString videoContainerFormat;
    Ui::CameraWidget *ui;

    bool isCapturingImage;
    bool applicationExiting;

    void buttonConnectors();



signals:

private slots:
    void setCamera(const QCameraInfo &cameraInfo);
    void updateCameraState(QCamera::State state);
    void updateRecordTime();
    void updateCameraDevice(QAction *action);
    void updateCaptureMode();
    void displayCameraError();
    void updateRecorderState(QMediaRecorder::State state);
    void displayRecorderError();
    void setExposureCompensation(int index);
    void updateLockStatus(QCamera::LockStatus status, QCamera::LockChangeReason reason);
    void readyForCapture(bool ready);
    void processCapturedImage(int requestId, const QImage& img);
    void displayCapturedImage();
    void imageSaved(int id, const QString &fileName);
    void displayCaptureError(int id, const QCameraImageCapture::Error error, const QString &errorString);
    void displayViewfinder();
    void takeImage();
    void stop();
    void record();
    void pause();
    void startCamera();
    void stopCamera();
    void toggleLock();

//    void on_takeImageButton_clicked();

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent *event);
};

#endif // CAMERAWIDGET_H
